Basic config
============

 * Basic, best ist to rename `vimfiles` to `.vim` or check it out that way.

 * `_vimrc` file should also work for windows
   in unix create a `.vimrc` file with  

    	`source ~/.vim/_vimrc`

   in it and all local config after that

 * vim-plug is used for package management now: 
   - PlugUpgrade: upgrades vim-plug
   - PlugInstall: install the packages
   - PlugUpdate: update the installed packages

 * Vim 8 is required
