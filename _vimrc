" Important starting config for everything {{{1
"" no more vi {{{2
set nocompatible

" Bugfixed {{{1
"" modifyOtherKeys not working properly it seems {{{2
let &t_TI = ""
let &t_TE = ""

"" pathogen start {{{2
"disables
"execute pathogen#infect()
"
"" vim-plug {{{2
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugs-auto')

Plug 'junegunn/goyo.vim'
Plug 'LaTeX-Box-Team/LaTeX-Box'
"Plug 'scrooloose/syntastic'
Plug 'godlygeek/tabular'
Plug 'majutsushi/tagbar'
Plug 'xolox/vim-easytags'
Plug 'pangloss/vim-javascript'
Plug 'thinca/vim-localrc'
Plug 'xolox/vim-misc'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'msanders/snipmate.vim'
Plug 'tpope/vim-surround'
Plug 'dhruvasagar/vim-table-mode'
Plug 'vimwiki/vimwiki'
Plug 'vim-voom/VOoM'
Plug 'Shougo/neocomplete.vim'
Plug 'davidbeckingsale/writegood.vim'
Plug 'vim-scripts/a.vim'

Plug 'w0rp/ale'

" Unmanaged plugin (manually installed and updated)
Plug '~/.vim/plugs-manual'

" Initialize plugin system
call plug#end()
"" Note that some stuff is set automatically (syntax file type indent etc.)


" General vim stuff {{{1

"" let there be color {{{2
syntax enable
"" enable syn sync third with minlines {{{2
syntax sync minlines=10000
"syn sync fromstart
set redrawtime=2000
" if nothing else is set below
" Cornsilk a pale yellowish color
hi Normal ctermbg=230 
"colorscheme desert
"set background=light
if has('gui_running')
    """nothing now
endif
""" just in case i will use solarized
let g:solarized_contrast="normal"
let g:solarized_termcolors=256
let g:solarized_italic=1
if &background == "dark"
    colorscheme zenburn
else
    colorscheme solarized
    "" unset undercurl method otherwise undercurl wont fallback to underline
    set t_Cs=
endif
""" i use lightyellow background in terminals, the gray fg color is to low contrast for me
"if &background == "light"
"    highlight Normal ctermfg=black 
"endif

""" colors in diff are garish (this is a fix for the desert colordiff mode)
if &diff
    highlight DiffAdd term=reverse cterm=bold ctermbg=lightgreen ctermfg=black
    highlight DiffChange term=reverse cterm=bold ctermbg=cyan ctermfg=black
    highlight DiffText term=reverse cterm=bold ctermbg=lightgray ctermfg=black
    highlight DiffDelete term=reverse cterm=bold ctermbg=red ctermfg=black 
endif

"" better search {{{2
set ignorecase
set smartcase
set incsearch 
set hlsearch


"" better grep because global {{{2
set gdefault

"" file type plugins {{{2
set autoindent 
"set smartindent
filetype indent on
filetype plugin on
"" Turn off # to start of column for python
autocmd FileType python inoremap <buffer> # X#

""" filetype workarounds for different extensions
autocmd BufRead *.j2 set filetype=htmldjango
autocmd BufNewFile,BufRead *.{md,mkd,markdown} set filetype=markdown.pandoc
let g:pandoc#syntax#conceal#use=0
""" set nginx syntax
autocmd BufRead,BufNewFile /etc/nginx/*,/usr/local/nginx/conf/* set syntax=nginx

""" Voom {{{1
"filetype stuff 
let g:voom_ft_modes = { 'markdown.pandoc': 'markdown', 'python': 'python', 'py': 'python', 'vimwiki': 'vimwiki', 'tex': 'latex' }

""" vimwiki {{{1
""" temporary wiki stuff:
let g:vimwiki_ext2syntax = {'.mdw': 'markdown', '.wiki': 'media'}
" default 
let wiki_default = {}
let wiki_default.path = '~/.vimwiki/'
let wiki_default.ext = '.mdw'
let wiki_default.syntax = 'markdown'
"pydio
let wiki_pydio = {}
let wiki_pydio.path = '~/.vimwiki/'
let wiki_pydio.ext = '.mdw'
let wiki_pydio.syntax = 'markdown'
let g:vimwiki_list = [wiki_default]


""" better tabs -> tab width = 4 and smart + expand
set tabstop=4
set softtabstop=4
set shiftwidth=4
set smarttab 
set expandtab
""" makefiles need tabs
autocmd FileType make setlocal noexpandtab

""" brackt highlighting
set showmatch
""" tenth of seconds to blink for matching bracket
set mat=2 

""" ruler
set ruler 
set showcmd
set rulerformat=%50(%=%m\ (%{&ff}%(,%{&fenc}%))(%b\|0x%B)\ %y\ %l,%c%V\ %P%)

""" statusline
set statusline=%<%n:%f%m\ (%{&ff}%(,%{&fenc}%))(%b\ 0x%B)%(\ [%R%H%W]%)%=words:%{wordcount().words}\ %y\ %l,%c%V\ %P
set laststatus=2 """ Always show the status line

""" allow defines per modeline
set modeline
set modelines=5

""" shut up with error sounds
set noerrorbells
set novisualbell
set t_vb=


""" linebreak and visual line movement with jk
set linebreak
set wrap
""" visual style movementfor text files only
nmap j gj
nmap k gk
vmap j gj
vmap k gk

""" utf
set encoding=utf-8
setglobal fileencoding=utf-8
set fileencodings=ucs-bom,utf-8,latin1
""" set default to unix file endings
set fileformats=unix,dos,mac

""" turn of backup, because it's annoying and git
set nobackup
set nowb
set noswapfile

""" backspace fix
set backspace=indent,eol,start

""" mouse for shell :help mouse
set mouse=a

""" autoresume on last edited line
autocmd BufRead * '"


""" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

"" mapleader settings {{{1
""" other maps 
let mapleader = ","
""" local maps
let maplocalleader = ";"

imap <leader>k <C-k>

nmap <leader>pp :set paste!<cr>
nmap <leader>ee :cope<cr>
nmap <leader>ss :set spell!<cr>
nmap <leader>nn :set number! relativenumber!<cr>

""writegood toggle
nmap <leader>wg :WritegoodToggle<cr>

"" DIGRAPH {{{1
dig -. 8230 "U+2026=… HORIZONTAL ELLIPSIS


""" surround motion text with char
"function! OPSurround(type, ...)
"    let schar=nr2char(getchar())
"    if schar =~ "\<ESC>" || schar =~ "\<C-C>"
"        return
"    endif
"    if a:0
"        let ps = getpos("'<")
"        let pe = getpos("'>")
"    else
"        let ps = getpos("'[")
"        let pe = getpos("']")
"    endif
"    let save_cursor = getpos(".")
"    call setpos(".", ps)
"    exe 's/[^[:space:][:punct:]]*\%#/'.schar.'&/'
"    call setpos(".", pe)
"    exe 's/\%#[^[:space:][:punct:]]*/&'.schar.'/'
"    call setpos(".", save_cursor)
"endfunction
"vmap <leader>s :call OPSurround(visualmode(), 1)<CR>
"nmap <leader>s :set opfunc=OPSurround<CR>g@
"""switched to tpopes surround.vim
nmap <leader>s ysiw
"""markdown specifics:
let b:surround_{char2nr('b')} = "__\r__"
let b:surround_{char2nr('B')} = "**\r**"
let b:surround_{char2nr('i')} = "_\r_"
let b:surround_{char2nr('I')} = "*\r*"
nmap <leader>sdb ds_ds_
nmap <leader>sdi ds_
nmap <leader>sdB ds*ds*
nmap <leader>sdI ds*


"""" file type mappings
function! MAPPINGmarkdown()
    map <buffer> <leader>h1 yyp\|:s/./=/<CR>\|:nohlsearch<CR>
    map <buffer> <leader>h2 yyp\|:s/./-/<CR>\|:nohlsearch<CR>
    imap <buffer> <leader>h1 <ESC><leader>h1o
    imap <buffer> <leader>h2 <ESC><leader>h2o
    """"  table-mode
    let g:table_mode_corner="|"
    let g:table_mode_header_fillchar="-"    
    """ Options
    set fo=ct2
    """" comments
    map <leader><leader>C :s/.\+/[comment]: \/ (&)/<CR>:nohls<CR>
    map <leader><leader>c :s/.*/[comment]: \/ ( & )/<CR>:nohls<CR>
    map <leader><leader>u :s/\[comment\]: \/ (\( \=\)\(.*\)\1)/\2/<CR>:nohls<CR>
endfunction
autocmd filetype markdown call MAPPINGmarkdown()

function! MAPPINGrestructuredtext()
    map <buffer> <leader>hp yyP\|:s/./#/<CR>\|<DOWN>p\|:s/./#/<CR>\|:nohlsearch<CR>
    map <buffer> <leader>hc yyP\|:s/./*/<CR>\|jp\|:s/./*/<CR>\|:nohlsearch<CR>
    map <buffer> <leader>h1 yyp\|:s/./=/<CR>\|:nohlsearch<CR>
    map <buffer> <leader>h2 yyp\|:s/./-/<CR>\|:nohlsearch<CR>
    map <buffer> <leader>h3 yyp\|:s/./^/<CR>\|:nohlsearch<CR>
    map <buffer> <leader>h4 yyp\|:s/./"/<CR>\|:nohlsearch<CR>
    imap <buffer> <leader>hp <ESC><leader>hpo
    imap <buffer> <leader>hc <ESC><leader>hco
    imap <buffer> <leader>h1 <ESC><leader>h1o
    imap <buffer> <leader>h2 <ESC><leader>h2o
    imap <buffer> <leader>h3 <ESC><leader>h3o
    imap <buffer> <leader>h4 <ESC><leader>h4o
    """"  table-mode
    let g:table_mode_corner_corner="+"
    let g:table_mode_header_fillchar="="    
endfunction
autocmd filetype rst call MAPPINGrestructuredtext()

" per PACKAGE config {{{1
"" table-mode {{{2
let g:table_mode_auto_align = 0 "cursor is not adjusted corretly with text
""" allow for reverse table sort also
nmap <Leader>tS :TableSort!<CR>

"" latex-box config {{{2
""" warnings will not show up as errors in quickfix window
let g:LatexBox_show_warnings=0 
""" quickfix will open but not steal focus
let g:LatexBox_quickfix =2

"" tabular {{{2
nmap <Leader>a= :Tabularize /=<CR>
vmap <Leader>a= :Tabularize /=<CR>
nmap <Leader>a& :Tabularize /&<CR>
vmap <Leader>a& :Tabularize /&<CR>
nmap <Leader>a: :Tabularize /:<CR>
vmap <Leader>a: :Tabularize /:<CR>
nmap <Leader>a| :Tabularize /|<CR>
vmap <Leader>a| :Tabularize /|<CR>
nmap <Leader>a, :Tabularize /,<CR>
vmap <Leader>a, :Tabularize /,<CR>
"""exclude char for tab
nmap <Leader>ae= :Tabularize /=\zs<CR>
vmap <Leader>ae= :Tabularize /=\zs<CR>
nmap <Leader>ae& :Tabularize /&\zs<CR>
vmap <Leader>ae& :Tabularize /&\zs<CR>
nmap <Leader>ae: :Tabularize /:\zs<CR>
vmap <Leader>ae: :Tabularize /:\zs<CR>
nmap <Leader>ae| :Tabularize /|\zs<CR>
vmap <Leader>ae| :Tabularize /|\zs<CR>
nmap <Leader>ae, :Tabularize /,\zs<CR>
vmap <Leader>ae, :Tabularize /,\zs<CR>
""" for taglist, moved to tagbar
""" nmap <Leader>ll :TlistToggle<CR>
nmap <Leader>ll :TagbarToggle<CR>
nmap <Leader>vv :Voom<CR>
nmap <Leader>df :Goyo<CR>
"" exit when only the taglist window is open
let Tlist_Exit_OnlyWindow=1

"" easytag {{{2
"""Tell easytag to use global if isntalled
if executable('/usr/bin/global') 
    let g:easytags_cmd='/usr/bin/global'
endif
"" prevent stalls for bigger files
let g:easytags_async=1

"" syntastic {{{2
""" pyflakes default highlight makes it so some text can't be read
highlight pyflakes cterm=reverse
"" pylint is the syntastic default and is way to picky for me
let g:syntastic_python_checkers = ['pyflakes']
let g:syntastic_cpp_checkers = ['cppcheck']
nmap <Leader>cc :SyntasticCheck<CR>
let g:syntastic_always_populate_loc_list=1

"" ale {{{2
""" delay in milliseconds after typing to send to checker
let g:ale_completion_delay = 1000
nmap <Leader>an :ALENext<CR>
nmap <Leader>ap :ALEPrevious<CR>
nmap <Leader>af :ALEFix<CR>
"" linters {{{3
let g:ale_linters={
            \    'python':['flake8', 'mypy'],
            \    }
""" fixers {{{3
let g:ale_fixers={
            \   'python': ['autopep8'],
            \   'java': ['uncrustify'],
            \    }
""" language config {{{3
"""" python {{{4
let g:ale_python_flake8_options="--ignore E501,E265"
let g:ale_python_autopep8_options="--ignore E501,E265"
"""" latex {{{4
let g:ale_tex_chktex_options="-n 36 -n 2 -n 10"

"" Neocomplete {{{2
""" disabled by default, toogle 
nmap <leader>nc :NeoCompleteToggle<CR>
""" settings
let g:neocomplete#enable_auto_close_preview=1

"" Other keybinds {{{2
""" in case you really wanted to sudo vim but forgot the sudo
cmap w!!  exec 'w !sudo tee >/dev/null '. shellescape(expand('%')) \| set nomodified


" GUI
set guifont=Mensch\ 12


" VIMDIFF
"" enable line wrp {{{2
autocmd VimEnter * if &diff | execute 'windo setlocal wrap<' | endif

